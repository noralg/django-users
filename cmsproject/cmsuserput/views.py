from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Contenido
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.shortcuts import redirect

formulario = """
<form action="" method="POST"
    valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""
# Create your views here.
def index(request):
    content_list = Contenido.objects.all()
    template = loader.get_template('cmsuserput/index.html')
    # ligar las variables de la plantilla a las variables de Python
    contexto = {
        'content_list': content_list
    }
    return HttpResponse(template.render(contexto, request))

@csrf_exempt
def get_content(request, llave):
    if request.method in ["POST", "PUT"]:
        if request.method == "POST":
            value = request.POST['valor']
        else:
            value = request.body.decode('utf-8')
        # PUT. (similar a POST)
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = value
            c.save()
        except Contenido.DoesNotExist:
            if request.user.is_authenticated:
                c = Contenido(clave=llave, valor=value)
                c.save()
                response = "Contenido guardado"
            else:
                response = "No estás autenticado, por favor <a href=/login>Haz login</a>"
            return HttpResponse(response)
    # GET
    try:
        c = Contenido.objects.get(clave=llave)
        response = f"El valor de la llave es:{c.valor}"

    except Contenido.DoesNotExist:
        if request.user.is_authenticated:
            response = formulario
        else:
            response = "No estás autenticado, <a href=/login>Haz login</a>"
    return HttpResponse(response)

def loggedIn(request):
    if request.user.is_authenticated:
        response = "Bienvenido"+request.user.username
    else:
        response = "No estás autenticado, <a href=/login>Haz login</a>"
    return HttpResponse(response)

def logout_view(request):
    logout(request)
    return redirect("/login")

def imagen(request):
    template = loader.get_template("cmsuserput/plantilla.html")
    context = {}
    return HttpResponse(template.render(context, request))