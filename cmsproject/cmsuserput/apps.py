from django.apps import AppConfig


class CmsuserputConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "cmsuserput"
