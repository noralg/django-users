from django.db import models

# Create your models here.

class Contenido(models.Model):
    clave = models.CharField(max_length=64)
    valor = models.TextField()
    def __str__(self):
        return f"{str(self.id)}:{self.clave}-->{self.valor}"

class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE) #establecer many-to-one relationship. si se elimina un contenido se eliminarán tbn los comentarios asociados
    titulo = models.CharField(max_length=128)
    comentario = models.TextField()
    fecha = models.DateTimeField()
    def __str__(self):
        return f"{str(self.id)}:{self.clave}-->{self.valor}"
    def validar_contenido(self):
        if "Cuerpo" in self.comentario:
            return True
        else:
            return False
